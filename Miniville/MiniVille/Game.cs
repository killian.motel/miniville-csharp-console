﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MiniVille
{
    class Game
    {
        static int win_W;
        static int win_H;

        
        private Pile pile;
        public Player player = new Player();
        public Player ia = new Player(true);
        public List<Player> players;
        
        public Game()
        {
            win_W = Console.WindowWidth;
            win_H = Console.WindowHeight;

            pile = new Pile();

            players = new List<Player>
            {
                player,
                ia
            };

            foreach(Player p in players)
            {
                p.AddEstablishmentCard((EstablishmentCard)FindCard("Ferme"));
                p.AddEstablishmentCard((EstablishmentCard)FindCard("Champs de blé"));
            }
        }

        public Card FindCard(string cardName)
        {
            foreach (EstablishmentCard card in pile.establishmentStack)
            {
                if (card.GetName() == cardName)
                {
                    return card;
                }
            }
            foreach (MonumentCard card in player.monumentCards)
            {
                if (card.GetName() == cardName)
                {
                    return card;
                }
            }
            return null;
        }

        // LANCEMENT DU JEU
        public void OnRun()
        {
            int result = 0;
            string lastCard = "";
            
            List<string> selection = new List<string>()
            {
                "Acheter un établissement",
                "Acheter un monument",
                "Finir le tour"
            };
            Random rdm = new Random();
            DisplayGame();

            do
            {
                foreach (Player p in players)
                {
                    do
                    {
                        Player enemy;
                        if (players[0] == p) enemy = players[1];
                        else enemy = players[0];

                        Console.WriteLine("[TOUR {0}]", p.isIA ? "IA" : "Joueur");
                        bool hasRerolled = false;
                        int choice = -1;
                        do
                        {
                            if (choice == 1) hasRerolled = true;

                            Console.WriteLine("{0}...", p.dices.Count > 1 ? "Lancer les dés" : "Lancer le dé");
                            Console.ReadKey();

                            result = p.ThrowDices();
                            foreach (Dice dice in p.dices)
                            {
                                Console.WriteLine(dice.ToString());
                            }
                            Console.WriteLine("Résultat {0} : {1}", p.dices.Count > 1 ? "des lancer" : "du lancer", result);
                            Console.ReadKey();

                            if (p.monumentCards[3].isBuilt && !hasRerolled)
                            {
                                Console.WriteLine("Souhaitez vous relancer les dés ? ");
                                choice = SelectChoice(new List<string> { "Oui", "Non" });
                                DisplayGame();
                            }
                        } while (choice == 1 && !hasRerolled);
                        // Effet des cartes
                        Console.WriteLine();
                        if (!OnCardEffect(new List<Player> { p, enemy }, result)) { Console.WriteLine("Aucune carte déclenchée"); }
                        Console.ReadKey();

                        bool passed = false;
                        do
                        {
                            Console.Clear();
                            DisplayGame();
                            if (p.isIA)
                            {
                                bool canBuy = false;
                                do
                                {
                                    choice = rdm.Next(0, 4);
                                    switch (choice)
                                    {
                                        case 1: // Acheter un Établissement
                                            choice = rdm.Next(0, pile.establishmentStack.Count);
                                            lastCard = pile.establishmentStack[choice].name;
                                            canBuy = CanBuyCard(lastCard);
                                            if (canBuy) p.BuyEstablishmentCard((EstablishmentCard)FindCard(lastCard));
                                            passed = false;
                                            break;
                                        case 2: // Acheter un Monument
                                            choice = rdm.Next(0, p.monumentCards.Count);
                                            lastCard = p.monumentCards[choice].name;
                                            canBuy = CanBuyCard(lastCard);
                                            if (canBuy) p.BuyMonumentCard((MonumentCard)FindCard(lastCard));
                                            passed = false;
                                            break;
                                        case 3: // Finir le tour
                                            passed = true;
                                            break;
                                    }
                                } while (!canBuy && passed);
                            }
                            else
                            {
                                Console.Write("\nQue souhaitez-vous faire : ");
                                choice = SelectChoice(selection);
                                switch (choice)
                                {
                                    case 1: // Acheter un Établissement
                                        lastCard = SelectEstablishments();
                                        if (lastCard != null) p.BuyEstablishmentCard((EstablishmentCard)FindCard(lastCard));
                                        break;
                                    case 2: // Acheter un Monument
                                        lastCard = SelectMonuments();
                                        if (lastCard != null) p.BuyMonumentCard((MonumentCard)FindCard(lastCard));
                                        break;
                                    case 3: // Finir le tour
                                        passed = true;
                                        break;
                                }
                            }

                        } while (lastCard == null && !passed);

                        if (p.monumentCards[0].isBuilt)
                            if (p.dices.Count < 2) p.dices.Add(new Dice(6));

                        if (!passed) Console.WriteLine("{0} : {1}", p.isIA ? "L'adversaire achète" : "Vous achetez", lastCard);
                        else Console.WriteLine("{0} tour", p.isIA ? "L'adversaire passe son" : "Vous passez votre");
                        Console.ReadKey();
                        Console.Clear();
                        DisplayGame();
                    } while (p.monumentCards[2].isBuilt && p.dices[0].face == p.dices[1].face);
                }
                       
            } while (!player.hasEveryMonuments() && !ia.hasEveryMonuments());
            
            if(player.hasEveryMonuments()) { Console.WriteLine("Vous avez gagné la partie ! "); }
            else { Console.WriteLine("Vous avez perdu la partie..."); }
        }

        // ACTIVATION DES EFFETS
        public bool OnCardEffect(List<Player> players, int result)
        {
            bool tmp = false;
            foreach (EstablishmentCard card in players[0].establishmentCards)
            {
                if (card.activationCost[0] == result || card.activationCost[1] == result)
                {
                    pile.UseEffect(card.name, players[0], players[1], Turn.PlayerTurn);
                    tmp = true;
                }
            }
            foreach (EstablishmentCard card in players[1].establishmentCards)
            {
                if (card.activationCost[0] == result || card.activationCost[1] == result)
                {
                    pile.UseEffect(card.name, players[1], players[0], Turn.EnemyTurn);
                    tmp = true;
                }
            }
            return tmp;
        }

        // AFFICHAGE JEU
        public void DisplayGame()
        {
            foreach (Player p in players)
            {
                DisplayBoard(p);
            }
            Console.WriteLine("------------------------------------");
        }
        public void DisplayBoard(Player p) {
            // IA ou Joueur ?
            Console.WriteLine("{0} ({1}$) :", p.isIA ? "IA" : "Joueur", p.money);

            // Affichage des établissements
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Monuments : ");
            foreach(MonumentCard card in p.monumentCards)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("| "); 
                if (card.isBuilt) Console.ForegroundColor = ConsoleColor.White; 
                else Console.ForegroundColor = ConsoleColor.DarkGray; // Si monument non construit : couleur = gris foncé

                Console.Write("{0} ", card.name);
            }
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("|");

            // Affichage des établissements
            Console.Write("\nÉtablissements : ");
            string establishments = "";
            List<EstablishmentCard> tmp = p.establishmentCards;
            for (int i = 0; i < p.establishmentCards.Count; i++)
            {
                int nb = 1;
                for (int j = i + 1; j < tmp.Count; j++)
                {
                    if (tmp[i].name == tmp[j].name)
                    {
                        nb++;
                        tmp.RemoveAt(j);
                    }
                }
                establishments += nb + "x " + tmp[i].name + " "; // 

                if (tmp[i].activationCost[1] == 0) establishments += "(" + tmp[i].activationCost[0] + ")"; // Cout d'activation (soit 1 seul, soit 2)
                else establishments += "(" + tmp[i].activationCost[0] + " ~ " + tmp[i].activationCost[1] + ")";

                establishments += " | ";
            }
            Console.WriteLine(establishments + "\n");

        }

        // FONCTION DE VERIFICATION

        public bool CanBuyCard(string cardName)
        {
            if (FindCard(cardName).cost <= ia.money) return true;
            else return false;
        }

        // AFFICHAGE POUR SELECTIONNER
        public int SelectChoice(List<string> selects)
        {
            List<Tuple<int, int, string, Boolean>> purchase = new List<Tuple<int, int, string, Boolean>>();

            for (int i = 0; i < selects.Count; i++)
            {
                 if(i == 0) purchase.Add(new Tuple<int, int, string, Boolean>(win_W / 2, win_H / 2 + i, selects[i], true));
                 else purchase.Add(new Tuple<int, int, string, Boolean>(win_W / 2, win_H / 2 + i, selects[i], false));
            }
          
            Console.CursorVisible = false;

            return DisplaySimpleSelect(purchase);
        }

        public string SelectMonuments()
        {
            int count = 0;
            List<Tuple<int, int, Card, Boolean>> monuments = new List<Tuple<int, int, Card, Boolean>>();
            Console.CursorVisible = false;

            foreach (MonumentCard card in player.monumentCards)
            {
                count++;
                if(count == 1) monuments.Add(new Tuple<int, int, Card, Boolean>(10, count, card, true));
                else monuments.Add(new Tuple<int, int, Card, Boolean>(10, count, card, false));
            }

            return DisplayCardSelect(monuments);
        }

        public string SelectEstablishments()
        {
            int count = 1;
            bool canAdd = true;
            List<Tuple<int, int, Card, Boolean>> establishment = new List<Tuple<int, int, Card, Boolean>>();
            Console.CursorVisible = false;

            foreach (EstablishmentCard card in pile.establishmentStack)
            {
                if (count == 1)
                {
                    establishment.Add(new Tuple<int, int, Card, Boolean>(10, count, card, true)); 
                    count++;
                }
                else
                {
                    foreach (Tuple<int, int, Card, Boolean> tuple in establishment)
                    {
                        if (tuple.Item3 == card) { canAdd = false; }
                    }
                    if (canAdd)
                    {
                        establishment.Add(new Tuple<int, int, Card, Boolean>(10, count, card, false));
                        count++;
                    }
                    canAdd = true;
                }
                    
            }

            return DisplayCardSelect(establishment);
        }

        public string DisplayCardSelect(List<Tuple<int, int, Card, Boolean>> selection) // Affichage de la sélection des monuments ou établissements
        {
            int slct = 0;
            
            while (true)
            {
                foreach (Tuple<int, int, Card, Boolean> tupe in selection)
                {
                    if (tupe.Item4 == true)
                    {
                        //sets the variable 'slct' to be equal to the index of the tuple value with the true value 
                        slct = selection.FindIndex(t => t.Item3 == tupe.Item3);
                        Console.SetCursorPosition(tupe.Item1, tupe.Item2);
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.Write(tupe.Item3.cost + "$ - " + tupe.Item3.name);
                    }
                    else if (tupe.Item4 == false)
                    {
                        Console.SetCursorPosition(tupe.Item1, tupe.Item2);
                        if (FindCard(tupe.Item3.name).cost > player.money){
                            Console.ForegroundColor = ConsoleColor.DarkGray;
                            if (tupe.Item3.GetType() == typeof(EstablishmentCard))
                            {
                                switch (((EstablishmentCard)tupe.Item3).colour)
                                {
                                    case "Vert":
                                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                                        break;
                                    case "Bleu":
                                        Console.ForegroundColor = ConsoleColor.DarkBlue;
                                        break;
                                    case "Rouge":
                                        Console.ForegroundColor = ConsoleColor.DarkRed;
                                        break;
                                    case "Violet":
                                        Console.ForegroundColor = ConsoleColor.DarkMagenta;
                                        break;
                                }
                            }
                        } else {
                            Console.ForegroundColor = ConsoleColor.White;
                            if (tupe.Item3.GetType() == typeof(MonumentCard))
                            {
                                foreach(MonumentCard card in player.monumentCards)
                                    if(card.name == tupe.Item3.name && card.isBuilt) Console.ForegroundColor = ConsoleColor.DarkGray;
                            }
                           
                            
                            if (tupe.Item3.GetType() == typeof(EstablishmentCard))
                            {
                                switch (((EstablishmentCard)tupe.Item3).colour)
                                {
                                    case "Vert":
                                        Console.ForegroundColor = ConsoleColor.Green;
                                        break;
                                    case "Bleu":
                                        Console.ForegroundColor = ConsoleColor.Cyan;
                                        break;
                                    case "Rouge":
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        break;
                                    case "Violet":
                                        Console.ForegroundColor = ConsoleColor.Magenta;
                                        break;
                                }
                            }
                        }
                        
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.Write(tupe.Item3.cost + "$ - " + tupe.Item3.name);
                        if (tupe.Item3.GetType() == typeof(EstablishmentCard))   
                            if(((EstablishmentCard)tupe.Item3).activationCost[1] == 0)
                                Console.Write(" (" + ((EstablishmentCard)tupe.Item3).activationCost[0] + ")");
                            else    
                                Console.Write(" (" + ((EstablishmentCard)tupe.Item3).activationCost[0] + " ~ " + ((EstablishmentCard)tupe.Item3).activationCost[1] + ")");
                    }
                }
                Console.SetCursorPosition(selection[1].Item1 + 1, selection[1].Item2);
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.Write("");
                string inp = Console.ReadKey().Key.ToString();
                if (inp == "UpArrow" && slct > 0)
                {
                    selection[slct] = new Tuple<int, int, Card, bool>(selection[slct].Item1, selection[slct].Item2, selection[slct].Item3, false);
                    slct -= 1;
                    selection[slct] = new Tuple<int, int, Card, bool>(selection[slct].Item1, selection[slct].Item2, selection[slct].Item3, true);
                }
                else if (inp == "DownArrow" && slct < selection.Count - 1)
                {
                    selection[slct] = new Tuple<int, int, Card, bool>(selection[slct].Item1, selection[slct].Item2, selection[slct].Item3, false);
                    slct += 1;
                    selection[slct] = new Tuple<int, int, Card, bool>(selection[slct].Item1, selection[slct].Item2, selection[slct].Item3, true);
                }
                else if(inp == "LeftArrow")
                {
                    return null;
                }
                else if (inp == "Enter")
                {
                    if(FindCard(selection[slct].Item3.name).cost <= player.money) { 
                        if(selection[slct].Item3.GetType() == typeof(MonumentCard)) 
                        {
                            foreach (MonumentCard card in player.monumentCards) {
                                if (card.name == selection[slct].Item3.name && !card.isBuilt) {
                                    Console.Clear();
                                    return selection[slct].Item3.name;
                                }
                            }
                        }
                        else if(selection[slct].Item3.GetType() == typeof(EstablishmentCard))
                        {
                            Console.Clear();
                            return selection[slct].Item3.name;
                        }
                        
                    }
                }
            }
        }

        public int DisplaySimpleSelect(List<Tuple<int, int, string, Boolean>> selection) // Affichage de la sélection de simple string
        {
            int slct = 0;

            while (true)
            {
                foreach (Tuple<int, int, string, Boolean> tupe in selection)
                {
                    if (tupe.Item4 == true)
                    {
                        //sets the variable 'slct' to be equal to the index of the tuple value with the true value 
                        slct = selection.FindIndex(t => t.Item3 == tupe.Item3);
                        Console.SetCursorPosition(tupe.Item1, tupe.Item2);
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.Write(tupe.Item3);
                    }
                    else if (tupe.Item4 == false)
                    {
                        Console.SetCursorPosition(tupe.Item1, tupe.Item2);
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.Write(tupe.Item3);
                    }
                }
                Console.SetCursorPosition(selection[1].Item1 + 1, selection[1].Item2);
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.Write("");
                string inp = Console.ReadKey().Key.ToString();
                if (inp == "UpArrow" && slct > 0)
                {
                    selection[slct] = new Tuple<int, int, string, bool>(selection[slct].Item1, selection[slct].Item2, selection[slct].Item3, false);
                    slct -= 1;
                    selection[slct] = new Tuple<int, int, string, bool>(selection[slct].Item1, selection[slct].Item2, selection[slct].Item3, true);
                }
                else if (inp == "DownArrow" && slct < selection.Count - 1)
                {
                    selection[slct] = new Tuple<int, int, string, bool>(selection[slct].Item1, selection[slct].Item2, selection[slct].Item3, false);
                    slct += 1;
                    selection[slct] = new Tuple<int, int, string, bool>(selection[slct].Item1, selection[slct].Item2, selection[slct].Item3, true);
                }
                else if (inp == "Enter")
                {
                    Console.Clear();
                    return slct + 1;
                }
            }
        }
    }
}
