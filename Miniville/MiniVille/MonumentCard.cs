﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniVille
{
    class MonumentCard : Card
    {
        private MonumentEffect monumentEffect;
        public bool isBuilt;

        public MonumentCard(string name, int cost) : base(name, cost)
        {
            isBuilt = false;

            switch (name)
            {
                case "Parc d'attractions":
                    monumentEffect = MonumentEffect.DoubleDices_ThenPlayAgain;
                    break;
                case "Tour Radio":
                    monumentEffect = MonumentEffect.RerollDices_Once;
                    break;
                case "Gare":
                    monumentEffect = MonumentEffect.SpeEstablishmentEarnMore;
                    break;
                case "Centre commercial":
                    monumentEffect = MonumentEffect.ThrowTwoDices;
                    break;
            }
        }


    }
}
