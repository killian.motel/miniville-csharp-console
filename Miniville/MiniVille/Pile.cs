﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniVille
{
    class Pile
    {
        public Dictionary<EstablishmentCard, int> establishmentCards = new Dictionary<EstablishmentCard, int>()
        {
            { new EstablishmentCard(new int[2]{0, 1 }, "Bleu", "Champs de blé", 1, CardType.Champs), 6},
            { new EstablishmentCard(new int[2]{2, 0 }, "Bleu", "Ferme", 1, CardType.Elevage), 6},
            { new EstablishmentCard(new int[2]{5, 0 }, "Bleu", "Forêt", 3, CardType.Ressources), 6},
            { new EstablishmentCard(new int[2]{10, 0 }, "Bleu", "Verger", 3, CardType.Champs ), 6},
            { new EstablishmentCard(new int[2]{9, 0 }, "Bleu", "Mine", 6, CardType.Ressources), 6},

            { new EstablishmentCard(new int[2]{2, 3 }, "Vert", "Boulangerie", 1,CardType.Commerce), 6},
            { new EstablishmentCard(new int[2]{4, 0 }, "Vert", "Supérette", 2, CardType.Commerce), 6},
            { new EstablishmentCard(new int[2]{11, 12 }, "Vert", "Marché de fruits et légumes", 2, CardType.Local), 6},
            { new EstablishmentCard(new int[2]{8, 0 }, "Vert", "Fabrique de meubles", 3, CardType.Usine), 6},
            { new EstablishmentCard(new int[2]{7, 0 }, "Vert", "Fromagerie", 5, CardType.Usine), 6},

            { new EstablishmentCard(new int[2]{3, 0 }, "Rouge", "Café", 2, CardType.Restauration), 6},
            { new EstablishmentCard(new int[2]{9, 10 }, "Rouge", "Restaurant", 3, CardType.Restauration), 6},
            
            { new EstablishmentCard(new int[2]{6, 0 }, "Violet", "Stade", 6, CardType.Monument), 4},
            { new EstablishmentCard(new int[2]{6, 0 }, "Violet", "Centre d'affaires", 8, CardType.Monument), 4},
            { new EstablishmentCard(new int[2]{6, 0 }, "Violet", "Chaîne de télévision", 7, CardType.Monument), 4}
        };

        public static Dictionary<string, Action<Player, Player, Turn>> effects { get; set; } = new Dictionary<string, Action<Player, Player, Turn>>()
        {
            { "Champs de blé", (p1, p2, turn) => { p1.money++; Console.WriteLine("[Champs de blé] {0} rapporte 1$", p1.isIA ? "lui" : "vous"); } },
            { "Ferme", (p1, p2, turn) => { p1.money++; Console.WriteLine("[Ferme] {0} rapporte 1$", p1.isIA ? "lui" : "vous");} },
            { "Boulangerie", (p1, p2, turn) => { if(turn == Turn.PlayerTurn) p1.money++; Console.WriteLine("[Boulangerie] {0} rapporte 1$", p1.isIA ? "lui" : "vous");} },
            { "Café", (p1, p2, turn) => {
                if(turn == Turn.EnemyTurn) {
                    if(p2.money >= 1)
                    {
                        p1.money++; p2.money--;
                    }
                    Console.WriteLine("[Café] {0} vole 1$", p1.isIA ? "lui" : "vous");
                }
            } },
            { "Supérette", (p1, p2, turn) => { if(turn == Turn.PlayerTurn) p1.money += 3; Console.WriteLine("[Café] {0} rapporte 3$", p1.isIA ? "lui" : "vous"); } },
            { "Forêt", (p1, p2, turn) => { p1.money++; Console.WriteLine("[Forêt] {0} rapporte 1$", p1.isIA ? "lui" : "vous");} },
            { "Stade", (p1, p2, turn) => { 
                 if(p2.money >= 2)
                {
                    p1.money += 2; p2.money -= 2;
                }
                 Console.WriteLine("[Stade] {0} vole 2$", p1.isIA ? "lui" : "vous");
            } },
            { "Centre d'affaires", (p1, p2, turn) => { // Échange de cartes
                
            } },
            { "Chaîne de télévision", (p1, p2, turn) => {
                if(turn == Turn.EnemyTurn) {
                    if(p2.money >= 5)
                    {
                        p1.money += 5; p2.money -= 5;
                    }
                    Console.WriteLine("[Chaîne de télévision] {0} vole 5$", p1.isIA ? "vous" : "lui");
                }
            } },
            { "Fromagerie", (p1, p2, turn) => {
                if(turn == Turn.PlayerTurn) {
                    int nb = 0;
                    foreach(EstablishmentCard card in p1.establishmentCards)
                    {
                        if(card.cardType == CardType.Elevage) nb++;
                    }
                    p1.money += nb * 3;
                    Console.WriteLine("[Fromagerie](x{0}) {1} rapporte {2}", nb, p1.isIA ? "lui" : "vous", nb * 3);
                }
            } },
            { "Fabrique de meubles", (p1, p2, turn) => {
                if(turn == Turn.PlayerTurn) {
                    int nb = 0;
                    foreach(EstablishmentCard card in p1.establishmentCards)
                    {
                        if(card.cardType == CardType.Usine) nb++;
                    }
                    p1.money += nb * 3;
                    Console.WriteLine("[Fabrique de meubles](x{0}) {1} raporte {2}", nb, p1.isIA ? "lui" : "vous", nb * 3); 
                }
            } },
            { "Mine", (p1, p2, turn) => { p1.money += 5; Console.WriteLine("[Mine] {0} rapporte 5$", p1.isIA ? "lui" : "vous");} },
            { "Restaurant", (p1, p2, turn) => {
                if(turn == Turn.EnemyTurn) {
                    if(p2.money >= 2)
                    {
                        p1.money += 2; p2.money -= 2;
                    }
                    Console.WriteLine("[Restaurant] {0} vole 2$", p1.isIA ? "vous" : "lui");
                }
            } },
            { "Verger", (p1, p2, turn) => { p1.money += 3; Console.WriteLine("[Verger] {0} rapporte 3$", p1.isIA ? "lui" : "vous");} },
            { "Marché de fruits et légumes", (p1, p2, turn) => {
                if(turn == Turn.PlayerTurn) {
                    int nb = 0;
                    foreach(EstablishmentCard card in p1.establishmentCards)
                    {
                        if(card.cardType == CardType.Ressources) nb++;
                    }
                    p1.money += nb * 2;
                    Console.WriteLine("[Marché de fruits et légumes](x{0}) {1} raporte {2}", nb, p1.isIA ? "lui" : "vous", nb * 3);
                }
            } }
        };

        public List<EstablishmentCard> establishmentStack = new List<EstablishmentCard>();

        public Pile()
        {
            foreach(KeyValuePair<EstablishmentCard, int> item in establishmentCards)
            {
                for (int i = 0; i < item.Value; i++)
                {
                    establishmentStack.Add(item.Key);
                }
            }
        }

        public void UseEffect(string cardName, Player p1, Player p2, Turn turn)
        {
            effects[cardName](p1, p2, turn);
            
        }

    }
}
