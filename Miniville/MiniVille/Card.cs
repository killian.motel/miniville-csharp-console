﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text;

namespace MiniVille
{

    class Card
    {
        public string name;
        public int cost;


        public string GetName()
        {
            return name;
        }
        public Card(string name, int cost)
        {
            this.name = name;
            this.cost = cost;
        }
    }
}
