﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace MiniVille
{
    class Dice
    {
        int nbFace;
        public int face;

        public Dice(int nbFace)
        {
            this.nbFace = nbFace;
            Throw();
        }

        public override string ToString()
        {
            string toString = String.Format("+---+\n");
            toString += String.Format("| {0} | \n", face);
            toString += String.Format("+---+");

            return toString;
        }

        public void Throw()
        {
            Random rdm = new Random();
            face = rdm.Next(1, nbFace + 1);
        }
    }
}
