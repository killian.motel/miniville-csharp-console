Controles :
 - Pour sélectionner une option : Flèche de haut / Flèche de bas
 - Pour retourner en arrière : Flèche de gauche

Vous jouez contre l'IA et la partie est finie lorsque l'un des joueurs a tous les monuments en sa possession.

 - les cartes, leurs coûts d'activation, leurs effets et leurs coûts d'achat:

 - (Bleu) Champs de blé; 1; Pendant le tour de n'importe quel joueur, recevez une pièce de la banque; 1.
 - (Bleu) Ferme; 2; Pendant le tour de n'importe quel joueur, recevez une pièce de la banque; 1.
 - (Vert) Boulangerie; 2 -3; Pendant votre tour uniquement, recevez une pièce de la banque; 1.
 - (Rouge) Café; 3; Recevez une pièce du joueur qui a lancé les dés; 2.
 - (Vert) Supérette; 4; Pendant votre tour uniquement, Recevez 3 pièces de la banque; 2.
 - (Bleu) Forêt; 5; Pendant le tour de n'importe quel joueur, recevez une pièce de la banque; 3.
 - (Violet) Centre d'affaires; 6; Pendant votre tour uniquement, vous pouvez échanger avec le joueur de votre choix un établissement qui ne soit pas de couleur violet; 6.
 - (Violet) Stade; 6; Pendant votre tour uniquement, recevez 2 pièces de la part de chaque autre joueur; 6.
 - (Violet) Chaîne de télévision; 6; Pendant votre tour uniquement, recevez 5 pièces du joueur de votre choix; 7.
 - (Vert) Fromagerie; 7; Pendant votre tour uniquement, recevez 3 pièces de la banque pour chaque Ferme que vous possédez; 5.
 - (Vert) Fabrique de meubles; Pendant votre tour uniquement, recevez 3 pièces de la banque pour chaque Forêt ou Mine que vous possédez; 3.
 - (Bleu) Mine; Pendant le tour de n'importe quel joueur, recevez 5 pièces de la banque; 6.
 - (Rouge) Restaurant; 9 - 10; Recevez 2 pièces du joueur qui a lancé les dés; 3.
 - (Bleu) Verger; 10; Pendant le tour de n'importe quel joueur, recevez 3 pièces de la banque; 3.
 - (Vert) Marché de fruits et légumes; 11 - 12; Pendant votre tour uniqment, recevez 2 pièces de la banque pour chaque champs de blé ou Verger que vous possédez; 2.

 - (Monument) Parc d'attraction; Passif; Si votre jet de dés est un double, rejouez un tour après celui-ci; 16.
 - (Monument) Tour Radio; Passif; Une fois par tour, vous pouvez choisir de relancer vos dés; 22;
 - (Monument) Gare; Passif; Vous pouvez lancer deux dés; 4.
 - (Monument) Centre commercial; Passif; Vos cafés, restaurants, boulangeries et supérettes vous rapportent une pièce de plus; 10.